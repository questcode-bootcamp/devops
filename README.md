Project from the course ****Bootcamp: Microserviços, Docker, Kubernetes, Jenkins e Helm.****

It is a social network developed as three microservices.

* ****FrontEnd**** - Is a microservice that contains all the web pages of the project. It connects to the BackEnds User (DB) and SCM to request information.
* ****SCM**** - Is a microservice that links your Git account to show your projects on your Dashboard.
* ****User**** - Is the microservice that connects to the DB (MongoDB) on the cloud.

All of them run on a Docker environment using Kubernetes as the orchestrator on AWS. Also Jenkins, Helm and GitLab were utilized for CI/CD. 