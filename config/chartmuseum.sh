#!/bin/bash

helm lint ../user-backend/
helm push ../user-backend/ questcode

helm lint ../scm-backend/
helm push ../scm-backend/ questcode

helm lint ../frontend/
helm push ../frontend/ questcode

helm repo update